var socket = io();
var timerInputFromUser;
var lastPick;
var choosenUserName;
var choosenUserNameConvinced;
var players;

//start a new game
$(document).on("click", '#newGame', function() {
 socket.emit('newGame');    
});

$(document).on("click", '#start', function(){
  socket.emit('startGame');
});

socket.on('removeStartButton', function(){
  $('#start').remove();
  $('#playersParagraph').text('In Game:');
  $('#sentence').remove();
  $('#newGame').prop('disabled', true);
  $('.homeButtons').append("<button id='newRound' class='side' disabled>Start Round</button>");
  $('#gameHeader').text('Mafia');
});

socket.on('loadGamePage', function(data){
  $("#pageContent").load("game.html");
  
});




//send client's name to server
$("#userName").on("click", function(){
  var userName = $("#username").val();
  if(userName != ""){
    $("#userParagraphName").remove();
    $("#helloUsername").append("<h3 id='helloh3'>Hello " + userName + "</h3>");
    socket.emit('addUserName', userName);
  }
});


//notify player for his role in the game
socket.on('role', function(player) {
  var roleStr = "You were choosen to be: ";
  $(".removeButtons").remove();
  $(".btn-group .mobile").css("display", "inline");

  if(player.role === 1){
    roleStr += "Murderer";
  }
  else if(player.role === 2){
    roleStr += "Detective";
  }
  else {
    roleStr += "Citizen";
  }
  $("#gameHeader").append("<div id='helloUserInGame'>Hello " + player.username + "</div>");
  $("#gameHeader").append("<div id='roleStr'>"+roleStr+"</div>");

  
});


//update clients list of players in the game screen
socket.on('updatescreen', function(data){
  updateScreen(data);
});


function updateScreen(data) {
 $(".players").remove();
 $("#playersParagraph").append("<ol class='players'></ol>");
 for(var i = 0 ; i < data.length ; i++){
  if(data[i].isAlive){
    $( ".players" ).append( "<li id="+data[i].id+">" + data[i].username + "</li>" );
  }
}
}
$(document).on("click", '#roundtime', function(){
  // console.log("clicked");
  // var timerInput = $("#num").val();
  // socket.emit('timerInput', timerInput);
});

socket.on('timerInput',function(timerInput){
  // timerInputFromUser = timerInput;
  console.log("updated timer input");
});

$(document).on("click", '#newRound', function(){
  // var timerInput = $("#num").val();
  socket.emit('newRound', 0);

});

//update timer
socket.on('timer', function(data){
  $("#newRound").text(data.minutes + ":" + data.seconds);
});   

//show list of players to choose from
socket.on('pickPlayer', function(data){
  choosenUserName = undefined;
  lastPick = undefined;
  $(".playersChoice").remove();
  $('#newRound').prop('disabled', true);
  if(!data.isNight){
    $("#roleStr").text('Round ' + data.roundNumber);
  }
  for(var i = 0 ; i < data.playersArr.length ; i++){
    if(data.playersArr[i].isAlive) {
        //if its night
        if(data.isNight === true){
       //if current client is the same player from the list -> disable button
       if(data.currentPlayer.username === data.playersArr[i].username){
        $("#chooseButtons").append("<button id="+data.playersArr[i].username+" type='button' class='list-group-item list-group-item-action MurderDetectiveChoice' disabled>" + data.playersArr[i].username + "</button>");
      } else{
        //If the current client is murderer
        if(data.currentPlayer.role === 1){
        //if its murderer notice the current client
        if(data.playersArr[i].role === 1){
          $("#chooseButtons").append("<button id="+data.playersArr[i].username+" type='button' class='list-group-item list-group-item-action MurderDetectiveChoice' disabled>" + data.playersArr[i].username + " -> Murderer with you!</button>");
        }
        else {
          $("#chooseButtons").append("<button id="+data.playersArr[i].username+" type='button' class='list-group-item list-group-item-action MurderDetectiveChoice'>" + data.playersArr[i].username + "</button>");
        }
        //not a murderer
      } else {
        $("#chooseButtons").append("<button id="+data.playersArr[i].username+" type='button' class='list-group-item list-group-item-action MurderDetectiveChoice'>" + data.playersArr[i].username + "</button>");
      }
    }
    //if its not a night -> regular round
  }
  if(data.isNight === false) {
    if(data.currentPlayer.username === data.playersArr[i].username){
      $("#chooseButtons").append("<button id="+data.playersArr[i].username+" type='button' class='list-group-item list-group-item-action playersChoice' disabled>" + data.playersArr[i].username + "<span id="+data.playersArr[i].username+"s class='badge badge-default badge-pill b'></span></button>");
    } else {
      $("#chooseButtons").append("<button id="+data.playersArr[i].username+" type='button' class='list-group-item list-group-item-action playersChoice'>" + data.playersArr[i].username + "<span id="+data.playersArr[i].username+"s class='badge badge-default badge-pill b'></span></button>");
    }
    
  }
}
}



$(".MurderDetectiveChoice").on("click", function(){

  if(data.currentPlayer.role === 1){
    socket.emit('choosen', $(this).attr('id'));
  }
  else {
    socket.emit('detectiveChoosen', $(this).attr('id'));
  }

    //mark as clicked and disable all
    $(this).addClass("active");
    $(".MurderDetectiveChoice").prop('disabled',true);
    $('#roleStr').text("");
  });



$(".playersChoice").on("click", function(){
  lastPick = choosenUserName;
  choosenUserName =  $(this).attr('id');

  socket.emit('pickedPlayerToKill', {choosenUserName: choosenUserName, lastPick: lastPick});

    //remove last pick(if there was one) mark as clicked 
    $(".playersChoice").removeClass("active");
    $(this).addClass("active");
  });

});



socket.on('playerPicked', function(data){
  $("#" + data.lastPick + "s").text(data.picksOfLastPick);
  $("#" + data.choosenUserName + "s").text(data.picks);
});


socket.on('detectiveChoise', function(detectiveChoise){
  console.log(detectiveChoise);
});

socket.on('nightEnds', function(nightResults){
  var resStr = "Results: ";
  
  //remove players choise list
  $(".MurderDetectiveChoice").remove();

  //Message the players for night results
  if(nightResults.killed === false){
    resStr += "No one was killed!";
  } else {
    resStr += "Im sorry but " + nightResults.killed + " was killed!";
  }

  resStr += " and.. ";
  if(nightResults.isMurderer === true){
    resStr += "The detective was right!";
  } else {
    resStr += "The detective was wrong!"; 
  }

  $("#nightResults").text(resStr);
  $('#newRound').prop('disabled', false);
});

//player got enough votes and get out of the game
socket.on('playerWasVoted', function(choosenUserName){
  //notify players who was voted
  $("#nightResults").text(choosenUserName + " was voted out of the game!");

  //remove players choise list
  $(".playersChoice").remove();

});

//choosen user name has last words
socket.on('lastWords', function(choosenUserName){
  choosenUserNameConvinced = choosenUserName;
  $('#nightResults').text(choosenUserName + " Last Words");
  $('#nightResults').append("<div id='lastWordsConvinced'>Did you convinced?</div>");
  $('#lastWordsConvinced').append("<div><button id='yesConvinced' class='convinced'>Yes</button><button id='noConvinced' class='convinced'>No</button></div>");
});

 $(document).on("click", '#yesConvinced', function(){
    console.log("yes convinced clicked");
    socket.emit('Convinced', {choice: true, choosenUserName: choosenUserNameConvinced});
    $('#nightResults').text("");
  });

  $(document).on("click", '#noConvinced', function(){
    socket.emit('Convinced',{choice: false, choosenUserName: choosenUserNameConvinced});
    $('#nightResults').text("");
  });

socket.on('convincedAccoured', function(){
  $('#nightResults').text("");
  $('.convinced').remove();
  $('#lastWordsConvinced').remove();
});


socket.on('gameOver', function(wins){
  $('#newGame').prop('disabled', false);
  $('#newRound').remove();
  $("#gameHeader").text(wins + " Wins!");
});

socket.on('getPlayersRoles', function(playersArr){
  console.log("entered getPlayerRoles");
  players = playersArr;
  $("#pageContent").html("<div id='playersRole' class='container text-center'></div>");
  $("#playersRole").append("<button id='getPlayersRoles'>Click For Players Roles</button>");
});

$(document).on("click", '#getPlayersRoles', function(){
  var roleStr = "";
  $(this).remove();
  $("#pageContent").append("<div id='roleh1' class='text-center'>Players Roles</div>");
  for(var i = 0 ; i < players.length ; i++){
    if(players[i].role === 1){
      roleStr = "Murderer";
    }
    else if(players[i].role === 2){
      roleStr = "Detective";
    }
    else {
      roleStr = "Citizen";
    }
    $("#roleh1").append("<div class='playersLabales'>" + players[i].username + " - " + roleStr + "</div>");
  }
})


//go to settings page
$("#settings").on("click", function(){
  $(".col-lg-12").load("settings.html");
});

$("#scores").on("click", function(){
  $(".col-lg-12").load("scores.html");
});

$("#rules").on("click", function(){
  $(".col-lg-12").load("rules.html");
});