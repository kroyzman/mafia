var gameSocket;
var sio;
var playersArr = [];
var countChoosen = 0;
var countNotConvinced = 0;
var countConvinced = 0;
var firstChoice;
var isMurderersChoosen = false;
var isDetectiveChoosen = false;
var nightResults = {killed: false, isMurderer: false};
var countRoles = {murderers: 0, detective: 0};
var roundNumber = 0;
var removedPlayer = false;

exports.resetGame = function(){
    for(var i = 0 ; i < playersArr.length ; i++){
        playersArr[i].role = 0;
        playersArr[i].picks = 0;
        playersArr[i].isAlive = true;
    }
    roundNumber = 0;
    countChoosen = 0;
    countNotConvinced = 0;
    countConvinced = 0;
    isMurderersChoosen = false;
    isDetectiveChoosen = false;
    countRoles.murderers = 0;
    countRoles.detective = 0;
    nightResults.killed = false;
    nightResults.isMurderer = false;
    removedPlayer = false;
    resetLastWords();
}

exports.checkUsernameInput = function(players){
    for(var i = 0 ; i < players.length ; i++){
        if(players[i].username === ""){
            return false;
        }
    }
    return true;
}

exports.setGame = function(io, players, SOCKET_LIST, socket){
    playersArr = players;

    //draw random roles
    drawRoles(playersArr);
    
    //send message to clients with their roles
    for(var i = 0 ; i < playersArr.length ; i++){
        gameSocket = SOCKET_LIST[playersArr[i].socketId];
        //send message to specific socket
        gameSocket.emit('role', playersArr[i]);
    }

    //Murderers and detecive choises
    initNight(io, playersArr, SOCKET_LIST);
    

    //Update clients screen
    io.sockets.emit('updatescreen', playersArr);
}

exports.startRound = function(io, players, timer, SOCKET_LIST){
    console.log("Convinced: " + countConvinced);
    playersArr = players;
    roundNumber++;
    countConvinced = 0;
    //timer
    //setRound(io, gameSocket, timer);

    //Show to client list of players to choose from(whos the murderer)
    for(var i = 0 ; i < playersArr.length ; i++){
        gameSocket = SOCKET_LIST[playersArr[i].socketId];
        gameSocket.emit('pickPlayer', {playersArr: players, currentPlayer: playersArr[i], isNight: false, roundNumber});    
    }                                                 
    
    
    //update clients screen
   //io.sockets.emit('updatescreen', playersArr);
}


//manage picks in regular round
exports.managePicks = function(io, choosenUserName, lastPick, playerArr, SOCKET_LIST){
    var lastPickPlayer;
    //if he changed his picks
    if(typeof lastPick != 'undefined'){      
        //get lastPick picks
        lastPickPlayer = getPlayerByName(lastPick);
        lastPickPlayer.picks--;
    }
    //its the first pick
    else {
        lastPickPlayer = getPlayerByName(choosenUserName);
    }
    

    for(var i = 0 ; i < playerArr.length ; i++){
        if(playersArr[i].username === choosenUserName){
            playersArr[i].picks++;
            //check if its enough picks
            if(checkPicks(playerArr[i].picks)){
                    //modify picks
                    io.sockets.emit('playerPicked', {choosenUserName: choosenUserName, picks: playersArr[i].picks, lastPick: lastPick, picksOfLastPick: lastPickPlayer.picks});
                //if there was last words already
                if(playerArr[i].lastWordsCommited){
                    //notify players
                    io.sockets.emit('playerWasVoted', choosenUserName);
                    //remove player   
                    removePlayer(io, SOCKET_LIST, choosenUserName);
                    //update clients screen
                    io.sockets.emit('updatescreen', playerArr);
                    //send to removed player roles 
                    gameSocket = SOCKET_LIST[playerArr[i].socketId];
                    gameSocket.emit('getPlayersRoles', playerArr); 
                    //init night
                    initNight(io, playersArr, SOCKET_LIST);
                    break;
                }
                //there wasnt last words
                else {
                    playerArr[i].lastWordsCommited = true;
                    io.sockets.emit('lastWords', choosenUserName);
                }  
            }   
            //not enough picks yet
            else {
                //modify picks
                io.sockets.emit('playerPicked', {choosenUserName: choosenUserName, picks: playerArr[i].picks, lastPick: lastPick, picksOfLastPick: lastPickPlayer.picks});
                break;
            }  
        }
    } 
}

function getPlayerByName(playerName){
    for(var i = 0 ; i < playersArr.length ; i++){
        if(playersArr[i].username === playerName){
            return playersArr[i];
        }
    }
}

//checks if theres enough votes for the current player
function checkPicks(currentPlayerPicks){
    if(currentPlayerPicks > (getNumOfPlayersAlive()/2)){
        return true;
    }

    return false;
}

//check if detective pick was correct
exports.checkChosenDetective = function(io, SOCKET_LIST, choosenUserName){
    var detectiveChoice = false;
    for(var i = 0 ; i < playersArr.length ; i++){
        if(playersArr[i].username === choosenUserName){
            //if its murderer
            if(playersArr[i].role === 1){
                detectiveChoice = true;
            }
            break;
        }
    }

    nightResults.isMurderer = detectiveChoice;
    isDetectiveChoosen = true;
    nightResultsFunc(io, removedPlayer, SOCKET_LIST);
}

//update murderer picks
exports.checkChosen = function(io, SOCKET_LIST, choosenUserName){

    countChoosen++;
    //2 murderers check if they both voted and if its the same name
    if(countRoles.murderers === 2){
        //only one voted
        if(countChoosen < countRoles.murderers){
            firstChoice = choosenUserName;
        //they both voted
    } else {
        isMurderersChoosen = true;
        if(firstChoice === choosenUserName){
          removedPlayer = removePlayer(io, SOCKET_LIST, choosenUserName);
      }
  }
} 
    //1 murderer
    else {
        isMurderersChoosen = true;
        removedPlayer = removePlayer(io, SOCKET_LIST, choosenUserName);
    }
    console.log("checkChosen: remove player - " + removedPlayer);
    nightResultsFunc(io, removedPlayer, SOCKET_LIST);
}


function removePlayer(io, SOCKET_LIST, choosenUserName){
    for(var i = 0 ; i < playersArr.length ; i++){

        if(playersArr[i].username === choosenUserName){
            nightResults.killed = choosenUserName;
            playersArr[i].isAlive = false;
            return playersArr[i];
        }
    }
}

function initNight(io, players, SOCKET_LIST){
 playersArr = players;
 console.log("entered initNight");
    //reset for new round
    countChoosen = 0;
    countRoles.murderers = 0;
    countRoles.detective = 0;
    isMurderersChoosen = false;
    countNotConvinced = 0;
    countConvinced = 0;
    removedPlayer = false;

    resetLastWords();
    //check how many murderers alive and if detective is alive
    countRoles = checkIfImportantRolesAlive();

    //check if game is over
    if(!checkGameStatus(io, countRoles)){ 

        //reset picks
        resetPicks();

        //reset for new round   
        nightResults.killed = false;
        nightResults.isMurderer = false;

        //detective still in the game
        if(countRoles.detective === 1){
            //reset choosen
            isDetectiveChoosen = false;
        }
        //detective is out of the game
        else {
            isDetectiveChoosen = true;
        }
        

        
        for(var i = 0 ; i < playersArr.length ; i++){
            gameSocket = SOCKET_LIST[playersArr[i].socketId];

            //If its murderer/detective -> choose player
            if(playersArr[i].role != 3){
                //choose from list of players
                gameSocket.emit('pickPlayer',{playersArr: playersArr, currentPlayer: playersArr[i], isNight: true});
            }
        }
    }
}

function resetLastWords(){
    for(var i = 0; i < playersArr.length ; i++){
        if(playersArr[i].isAlive){
            playersArr[i].lastWordsCommited = false;
        }
    }
}

exports.manageLastWords = function(io, SOCKET_LIST, choosenUserName, choice){
    console.log("First Convinced: " + countConvinced);
    //not convinced
    if(!choice){
        countNotConvinced++;
    }
    //convinced
    else {
        countConvinced++;
    }
    
    //Majority of players not convinced - remove player
    if(countNotConvinced > (getNumOfPlayersAlive()/2)){
      console.log("Not Convinced: " + countNotConvinced);
      console.log("getNumOfPlayersAlive in manageLastWords: " + getNumOfPlayersAlive());
      countNotConvinced = 0;
      countConvinced = 0;

      io.sockets.emit('playerWasVoted', choosenUserName);
      //remove player   
      removedPlayer = removePlayer(io, SOCKET_LIST, choosenUserName);
      //update clients screen
      io.sockets.emit('updatescreen', playersArr);

      gameSocket = SOCKET_LIST[removedPlayer.socketId];
      gameSocket.emit('getPlayersRoles', playersArr);
      
      //init night
      initNight(io, playersArr, SOCKET_LIST);
  }

    //Majority of players was convinced - reset picks
    if(countConvinced > (getNumOfPlayersAlive()/2)){
        
        countNotConvinced = 0;
        countConvinced = 0;
        
        console.log("getNumOfPlayersAlive in manageLastWords: " + getNumOfPlayersAlive());
        io.sockets.emit('convincedAccoured');
        resetPicks();
        roundNumber--;
        exports.startRound(io, playersArr, 0, SOCKET_LIST);
        
    }
}

function checkGameStatus(io, countRoles){
    //if all murderes is out of the game
    if(countRoles.murderers === 0){
        io.sockets.emit('gameOver', 'Citizens');
        return true;                                                             
    }

    //check if murderers won
    if(countRoles.murderers >= (getNumOfPlayersAlive()/2)){
        console.log("murderers wins!");
        io.sockets.emit('gameOver', 'Murderers');
        return true;
    }

    return false;

}

function getNumOfPlayersAlive(){
    var count = 0;
    for(var i = 0 ; i < playersArr.length ; i++){
        if(playersArr[i].isAlive){
            count++;
        }
    }

    return count;
}

//reset votes from last round
function resetPicks(){
    for(var i = 0 ; i < playersArr.length ; i++){
        playersArr[i].picks = 0;
    }
}

//returns count of murderes/detctive
function checkIfImportantRolesAlive(){

    for(var i = 0 ; i < playersArr.length ; i++){
        if(playersArr[i].isAlive){
            if(playersArr[i].role === 1){
                countRoles.murderers++;
            }
            if(playersArr[i].role === 2){
                countRoles.detective++;
            }
        }
    }
    return countRoles;
}



function nightResultsFunc(io, removedPlayer, SOCKET_LIST){
    //Wait for Night to end
    if(!isMurderersChoosen || !isDetectiveChoosen){
        return;
    }

     //results of night
     io.sockets.emit('nightEnds', nightResults);
     io.sockets.emit('updatescreen', playersArr);

     if(removedPlayer != false){
        console.log("entered removed player not undefined");
        gameSocket = SOCKET_LIST[removedPlayer.socketId];
        gameSocket.emit('getPlayersRoles', playersArr);
    }
     //check if game is over
     checkGameStatus(io, countRoles);
 }

//Set timer for each round
function setRound(io, socket, duration){
    var timer = duration, minutes, seconds;
    var intervalId = setInterval(function () {
        minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        if (--timer < 0) {
            clearInterval(intervalId);
            io.sockets.emit('endOfRound', playersArr);
        }
        
        io.sockets.emit('timer', {minutes: minutes, seconds: seconds});

    }, 1000);
}



function drawRoles(playersArr){
    var randomPlayerIndex;   
    var roles = {
        MURDERER:{
          value: 1,
          count: 0
      } ,
      DETECTIVE:{
          value: 2,
          count: 0
      },
      CITIZEN:{
          value: 3,
          count:0
      } 
  };

    //draw 2 murderers
    while(roles.MURDERER.count < 2){

        randomPlayerIndex = Math.round(Math.random()*(playersArr.length-1));
        
        if(playersArr[randomPlayerIndex].role === 0){
            playersArr[randomPlayerIndex].role = roles.MURDERER.value;
            roles.MURDERER.count++;
        }
        
    }
    
    //draw detective
    while(roles.DETECTIVE.count < 1){
        randomPlayerIndex = Math.round(Math.random()*(playersArr.length-1));
        
        if(playersArr[randomPlayerIndex].role === 0){
            playersArr[randomPlayerIndex].role = roles.DETECTIVE.value;
            roles.DETECTIVE.count++;
        }
    }
    
    //All the rest are citizens
    for(var i = 0 ; i < playersArr.length ; i++){
        if(playersArr[i].role === 0){
            playersArr[i].role = roles.CITIZEN.value;
            roles.CITIZEN.count++;
        }
    }
    
    
}
