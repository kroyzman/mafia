// Import the Express module.
var express = require('express');

// Import the 'path' module (packaged with Node.js)
var path = require('path');

// Create a new instance of Express
var app = express();

// Import the mafia game file.
var mafia= require('./mafiagame');

var faker = require('faker');
// Serve static js
app.use(express.static(__dirname + '/script'));
app.use(express.static(__dirname + '/public/css'));
app.use(express.static(__dirname + '/images'));
app.use(express.static(__dirname + '/public'));



app.get('/pickPlayers', function(req, res){
 res.sendFile(path.join(__dirname + '/public/pickplayers.html'));
});

app.get('/gameplay', function(req, res){
  res.sendFile(path.join(__dirname + '/public/index.html'));
});

// Create a Node.js based http server on port 8080
var server = require('http').createServer(app).listen(process.env.PORT || 5000, function(){
  console.log("Server Init");
});

// Create a Socket.IO server and attach it to the http server
var io = require('socket.io').listen(server);

// var players = [{id: 123, username: "nadi1", role: 0}, {id: 123, username: "nadi2", role: 0}, {id: 123, username: "nadi3", role: 0}, {id: 123, username: "nadi4", role: 0}, {id: 123, username: "nadi5", role: 0}, {id: 123, username: "nadi6", role: 0}, {id: 123, username: "nadi7", role: 0}];
var players = [];
var SOCKET_LIST = {};



io.sockets.on('connection', function(socket){
  console.log("connect");
  socket.id = Math.random();
  SOCKET_LIST[socket.id] = socket;
  socket.username = "";  //faker.name.firstName()
  var newPlayer = {socketId: socket.id, username: socket.username, role: 0, picks: 0, lastWordsCommited: false,  isAlive: true};
  players.push(newPlayer);
  
  socket.on('addUserName', function(data){
    for(var i = 0 ; i < players.length ; i++){
      if(players[i].socketId === socket.id){
        players[i].username = data;
        break;
      }
    }
    socket.username = data;
    io.sockets.emit('updatescreen', players);
  });


  socket.on('newGame', function(){
    if(mafia.checkUsernameInput(players) && players.length > 4){
      mafia.resetGame();
      io.sockets.emit('loadGamePage', players);
    }
  });

  socket.on('startGame', function(){
    io.sockets.emit('removeStartButton');
    mafia.setGame(io, players, SOCKET_LIST, socket);
  });

  socket.on('newRound', function(timer){
    mafia.startRound(io, players, timer, SOCKET_LIST);
  });

  socket.on('choosen', function(choosenUserName){
    mafia.checkChosen(io, SOCKET_LIST, choosenUserName);
  });

  socket.on('detectiveChoosen', function(choosenUserName){
    mafia.checkChosenDetective(io, SOCKET_LIST, choosenUserName);
  });

  socket.on('pickedPlayerToKill', function(data){
    mafia.managePicks(io, data.choosenUserName, data.lastPick, players, SOCKET_LIST);
  });

  socket.on('disconnect', function(){
    for(var i = 0 ; i < players.length ; i++){
      if(players[i].socketId === socket.id){
       players.splice(i, 1);
     }
   }
   delete SOCKET_LIST[socket.id];
   io.sockets.emit('updatescreen', players);
 });

  socket.on('timerInput', function(timerInput){

  });

  socket.on('Convinced', function(data){
    mafia.manageLastWords(io, SOCKET_LIST, data.choosenUserName, data.choice);
  });

  io.sockets.emit('updatescreen', players);
});